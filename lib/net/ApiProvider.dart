import 'package:dio/dio.dart';

import 'RootModel.dart';

class ApiProvider {
  final String url = "https://tastedive.com/api/similar?q=";
  final Dio dio = Dio();

  Future<List<Results>> getArtists(String artist) async {
    Response response = await dio.get(url + artist);
    return RootModel.fromJson(response.data)
        .similar
        .results;
  }
}
