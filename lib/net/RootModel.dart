class RootModel {
    Similar similar;

    RootModel({this.similar});

    RootModel.fromJson(Map<String, dynamic> json) {
        similar = json['Similar'] != null ? new Similar.fromJson(json['Similar']) : null;
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        if (this.similar != null) {
            data['Similar'] = this.similar.toJson();
        }
        return data;
    }
}

class Similar {
    List<Results> results;

    Similar({this.results});

    Similar.fromJson(Map<String, dynamic> json) {
        if (json['Results'] != null) {
            results = new List<Results>();
            json['Results'].forEach((v) {
                results.add(new Results.fromJson(v));
            });
        }
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        if (this.results != null) {
            data['Results'] = this.results.map((v) => v.toJson()).toList();
        }
        return data;
    }
}

class Results {
    String name;
    String type;

    Results({this.name, this.type});

    Results.fromJson(Map<String, dynamic> json) {
        name = json['Name'];
        type = json['Type'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['Name'] = this.name;
        data['Type'] = this.type;
        return data;
    }
}
