import 'package:flutter/material.dart';
import 'package:flutter_app_8/db/database_manager.dart';
import 'package:flutter_app_8/history.dart';
import 'package:flutter_stetho/flutter_stetho.dart';

import 'db/database.dart';
import 'main_bloc.dart';

void main() async {
  runApp(MyApp());

  final database = await $FloorAppDatabase
      .databaseBuilder('flutter_database.db')
      .build();
  DatabaseManager.initDatabase(database);
  Stetho.initialize();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: SearchList());
  }
}

class SearchList extends StatefulWidget {
  @override
  SearchListState createState() => SearchListState();
}

class SearchListState extends State<SearchList> {
  Widget appBarTitle = new Text("Search similar artists", style: new TextStyle(color: Colors.white),);
  Icon searchButton = new Icon(Icons.search, color: Colors.white,);
  final TextEditingController searchQuery = new TextEditingController();
  bool isSearching = false;
  List<String> artistList = List();
  String searchText = "";
  MainManager mainManager = MainManager();


  @override
  void dispose() {
    mainManager.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: buildAppBar(),
        body:showArtistsFromNetwork(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(centerTitle: true, title: appBarTitle, actions: <Widget>[
      new IconButton(
        icon: searchButton,
        onPressed: () {
          setState(() {
            if (!isSearching) {
              startSearch();
            } else {
              finishSearch();
            }
          });
        },
      ),
      new IconButton(
          icon: new Icon(Icons.history, color: Colors.white),
          onPressed: (){
              insertToDb();
              Navigator.push(context, MaterialPageRoute(builder: (context) => HistoryScreen()));
          })
    ]);
  }

  void insertToDb() {
    mainManager.insertToDb(artistList);
  }

  Widget showArtistsFromNetwork() {
   return  StreamBuilder(
      stream: mainManager.netRequestStream,
      builder: (context, AsyncSnapshot<List<String>> snapshot){
        artistList.clear();
        if(snapshot == null || !snapshot.hasData){
          return ListView();
        } else {
          artistList = snapshot.data;
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext ctx, int index){
            return new Text(snapshot.data[index]);
            },
          );
        }
      },
    );
  }

  SearchListState() {
    searchQuery.addListener(() {
      if (searchQuery.text.isEmpty) {
          isSearching = false;
          searchText = "";
      }
      else {
          isSearching = true;
          searchText = searchQuery.text;
          mainManager.loadData(searchText);
      }
    });
  }

  void startSearch() {
      searchButton = new Icon(Icons.close, color: Colors.white,);
      appBarTitle = new TextField(controller: searchQuery, style: new TextStyle(color: Colors.white,),
        decoration: new InputDecoration(prefixIcon: new Icon(Icons.search, color: Colors.white),
            hintText: "Search...",
            hintStyle: new TextStyle(color: Colors.white)),
      );
      isSearching = true;
  }

  void finishSearch() {
      searchButton = new Icon(Icons.search, color: Colors.white,);
      appBarTitle = new Text("Search Sample", style: new TextStyle(color: Colors.white),);
      isSearching = false;
      searchQuery.clear();
  }
}