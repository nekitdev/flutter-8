

import 'dart:async';

import 'package:flutter_app_8/db/database_manager.dart';
import 'package:flutter_app_8/db/entity/artist_entity.dart';

import 'net/ApiProvider.dart';


class MainManager  {

  final StreamController<List<String>> _netRequestController = StreamController.broadcast();
  Stream<List<String>> get netRequestStream => _netRequestController.stream;

  var apiProvider = ApiProvider();

//  final StreamController<List<String>> _dbInsertController = StreamController.broadcast();
//  Stream<List<String>> get dbInsertStream => _dbInsertController.stream;


  void loadData(String artists) async{
    await apiProvider.getArtists(artists).then((results){
      _netRequestController.add(results.map((results) => results.name).toList());
    });
  }

  void insertToDb(List<String> artistsList) async{
    await DatabaseManager().artistDao.deleteAllArtists();
    List<Artist> listArt = artistsList.map((name) => Artist(null, name)).toList();
    await DatabaseManager().artistDao.insertAllArtists(listArt);
  }

  void dispose(){
    _netRequestController?.close();
//    _dbInsertController?.close();
  }

}