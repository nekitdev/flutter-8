import 'dart:async';
import 'package:floor/floor.dart';
import 'package:flutter_app_8/db/dao/artist_dao.dart';
import 'package:flutter_app_8/db/entity/artist_entity.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart';

@Database(version: 1, entities: [Artist])
abstract class AppDatabase extends FloorDatabase {
  ArtistDao get artistDao;
}

