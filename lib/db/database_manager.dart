

import 'package:flutter_app_8/db/dao/artist_dao.dart';
import 'package:flutter_app_8/db/database.dart';

class DatabaseManager extends AppDatabase {
  static AppDatabase _sInstance;
  static final DatabaseManager _internal = DatabaseManager.internal();

  factory DatabaseManager() {
    return _internal;
  }

  DatabaseManager.internal();

  static AppDatabase get sInstance => _sInstance;

  static AppDatabase initDatabase(AppDatabase database) {
    if (_sInstance == null) {
      _sInstance = database;
    }
    return _sInstance;
  }

  @override
  ArtistDao get artistDao => _sInstance.artistDao;
}
