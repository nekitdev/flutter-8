import 'package:floor/floor.dart';


@entity
class Artist {
  @PrimaryKey(autoGenerate: true)
  final int id;

  final String name;

  Artist(this.id, this.name);
}