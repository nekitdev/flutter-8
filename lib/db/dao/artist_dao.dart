import 'package:floor/floor.dart';
import 'package:flutter_app_8/db/entity/artist_entity.dart';

@dao
abstract class ArtistDao {
  @Query('SELECT * FROM Artist')
  Future<List<Artist>> getAllArtists();

  @insert
  Future<void> insertAllArtists(List<Artist> artist);

  @Query('DELETE FROM Artist')
  Future<void> deleteAllArtists();
}