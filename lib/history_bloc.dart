

import 'dart:async';

import 'package:flutter_app_8/db/database_manager.dart';


class HistoryManager  {

  final StreamController<List<String>> _getAllArtists = StreamController.broadcast();
  Stream<List<String>> get dbGetAllStream => _getAllArtists.stream;


  void loadData() async{
    await DatabaseManager().artistDao.getAllArtists().then((results){
      _getAllArtists.add(results.map((results) => results.name).toList());
    });
  }

  void dispose(){
    _getAllArtists?.close();
  }

}