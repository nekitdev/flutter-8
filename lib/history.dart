import 'package:flutter/material.dart';
import 'package:flutter_app_8/history_bloc.dart';

class HistoryScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: HistoryList());
  }
}

class HistoryList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HistoryListState();
  }
}

class HistoryListState extends State<HistoryList> {

  HistoryManager historyManager = HistoryManager();

  @override
  void dispose() {
    historyManager.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    historyManager.loadData();

    return Scaffold(
      appBar: AppBar(
        title: Text("Show db"),
      ),
      body: showArtistsFromDatabase(),
    );
  }

  Widget showArtistsFromDatabase() {
    return  StreamBuilder(
      stream: historyManager.dbGetAllStream,
      builder: (context, AsyncSnapshot<List<String>> snapshot){
        if(snapshot == null || !snapshot.hasData){
          return ListView();
        } else {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext ctx, int index){
              return new Text(snapshot.data[index]);
            },
          );
        }
      },
    );
  }
}